# 🍐 AI Safety Ideas Forum

This project is the repository for the [Technical AI Safety Ideas repository](https://docs.google.com/spreadsheets/u/2/d/1Ep8Dd52c00kxWvu75btazHEdPSA7jVJwrY51jKfPSBo/edit#gid=719912604) and collection that will be curated and managed on the platform. From **this repository**, all CI/CD runs will be managed. The database runs on Supabase and the website will run on Netlify or Vercel.

## 🐵 Contributing to the project

Create a branch with your changes, send a pull request and we will accept your pull request.

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
